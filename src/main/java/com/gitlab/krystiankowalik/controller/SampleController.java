package com.gitlab.krystiankowalik.controller;

import com.gitlab.krystiankowalik.service.IpProvider;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

import java.io.IOException;

@Controller("/sampleMicronautMicroservice")
public class SampleController {

    private final IpProvider ipProvider;

    public SampleController(IpProvider ipProvider) {
        this.ipProvider = ipProvider;
    }

    @Get(uri = "/", produces = "text/plain")
    public String index() throws IOException {
        return "Example Response from " + ipProvider.getIpAddress();
    }
}